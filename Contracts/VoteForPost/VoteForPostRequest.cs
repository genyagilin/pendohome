﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts
{
    public class VoteForPostRequest
    {
        public int PostID { get; set; }
        public Vote Vote { get; set; }
    }
}
