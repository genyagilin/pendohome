﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts
{
    public class Vote
    {
        // NOTE: If i were to implement an actualy login/signup process this would be a user object
        public string UserName { get; set; }
        public int VoteValue { get; set; }
    }
}
