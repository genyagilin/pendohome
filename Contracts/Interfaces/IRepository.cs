﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.Interfaces
{
    public interface IRepository
    {
        //Task<APost> GetPost(string postID);
        Task<List<APost>> GetPosts(int FromPost, int ToPost);
        Task CreateOrUpdatePost(APost postToUpdateOrCreate);
        Task<bool> VoteForPost(int postID,Vote aVote);
    }
}
