﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts
{
    public class APost
    {
        public string ID { get; set; }
        public string Text { get; set; }
        public int Votes { get; set; }
        public DateTime CreationTime { get; set; }
    }
}
