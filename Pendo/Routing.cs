﻿using Contracts;

using Contracts.Interfaces;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Pendo
{
    public class Routing : NancyModule
    {
        IRepository _repository;
        public Routing(IRepository repository)
        {
            Get["/posts", true] = GetPosts;
            Post["/posts", true] = CreateOrUpdatePost;
            Post["/vote", true] = VoteForPost;

            _repository = repository;
        }

        private async Task<dynamic> VoteForPost(dynamic arg, CancellationToken cts)
        {
            try
            {
                var request = this.Bind<VoteForPostRequest>();

                var result = await _repository.VoteForPost(request.PostID, request.Vote);

                var response = new VoteForPostResponse();
                response.Success = result;

                return response;
                
            }
            catch (Exception ex)
            {
                return HttpStatusCode.InternalServerError;
            }
        }

        private async Task<dynamic> CreateOrUpdatePost(dynamic arg, CancellationToken cts)
        {
            try
            {
                var request = this.Bind<CreateOrUpdatePostRequest>();
                await _repository.CreateOrUpdatePost(request.Post);

                return HttpStatusCode.OK;
            }
            catch (Exception ex)
            {

                 return  HttpStatusCode.InternalServerError;
            } 

           
        }

        private async Task<dynamic> GetPosts( dynamic arg, CancellationToken cts)
        {
            var request = this.Bind<GetPostsRequest>();
            int getPostsFrom = 0;

            if (request.Page > 0)
            {
                getPostsFrom = request.Page * 100;
            }

            var posts = await _repository.GetPosts(getPostsFrom, getPostsFrom + 100);

            GetPostsResponse res = new GetPostsResponse();
            res.Posts = posts;

            return res;
        }
    }
}
