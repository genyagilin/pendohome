﻿using Contracts.Interfaces;
using Nancy;
using Nancy.Hosting.Self;
using Nancy.TinyIoc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Repository;
using Nancy.Bootstrapper;

namespace Pendo
{
    public class MyNancyBoot : DefaultNancyBootstrapper
    {
        protected override void ConfigureApplicationContainer(TinyIoCContainer container)
        {
            //base.ConfigureApplicationContainer(container);
            container.Register<IRepository>(new Repository.Repository());

        }

        protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
        {
            base.ApplicationStartup(container, pipelines);
        }

    }

    class Program
    {

        static void Main(string[] args)
        {
            var url = "http://127.0.0.1:9000";
            using (var host = new NancyHost(new Uri(url)))
            {
               
                host.Start();
                Console.WriteLine($"Pendo Test listening on {url}");
                Console.ReadLine();
            }
        }
    }
}
