﻿using Contracts;
using Contracts.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class Repository : IRepository
    {
        string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\Database.mdf;Integrated Security=True";
        public Repository()
        {
            
        }

        public async Task CreateOrUpdatePost(APost postToUpdateOrCreate)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    using (var command = conn.CreateCommand())
                    {
                        await conn.OpenAsync();

                        if (string.IsNullOrEmpty(postToUpdateOrCreate.ID))
                        {
                            command.CommandText = $"INSERT INTO Posts VALUES ('{postToUpdateOrCreate.Text}', CURRENT_TIMESTAMP)";
                        }
                        else
                        {
                            command.CommandText = $"UPDATE Posts SET text={postToUpdateOrCreate.Text}, creationtime=CURRENT_TIMESTAMP WHERE id={postToUpdateOrCreate.ID}";

                        }
                        await command.ExecuteNonQueryAsync();


                    }
                }
                catch (Exception ex)
                {

                }
               
            }
        }

        public async Task<List<APost>> GetPosts(int FromPost, int ToPost)
        {
            List<APost> posts = new List<APost>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    using (var command = conn.CreateCommand())
                    {
                        await conn.OpenAsync();


                        command.CommandText = $@"select  Posts.id, Posts.text, Posts.creationtime, ISNULL(votesperpost.voteamount,0) from Posts
                                                left join(select Votes.postid, count(Votes.value) as voteamount from Votes group by postid) votesperpost on Posts.Id = votesperpost.postid order by voteamount desc, creationtime desc offset {FromPost} ROWS FETCH NEXT {ToPost} ROWS ONLY ";

                       var reader = await command.ExecuteReaderAsync();

                        while (await reader.ReadAsync())
                        {

                            posts.Add(new APost() { ID = reader.GetInt32(0).ToString(), Text = reader.GetString(1), CreationTime = reader.GetDateTime(2), Votes = reader.GetInt32(3) });
                            
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }

            return posts;
        }

        public async Task<bool> VoteForPost(int postid, Vote aVote)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    using (var command = conn.CreateCommand())
                    {
                        await conn.OpenAsync();


                        command.CommandText = $@"Insert into Votes values('{aVote.UserName}', CURRENT_TIMESTAMP, {aVote.VoteValue}, {postid})";

                        await command.ExecuteNonQueryAsync();

                        return true;
                    }
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }
    }
}
